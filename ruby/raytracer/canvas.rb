require_relative 'color'

module Raytracer
  class Canvas
    INITIAL_COLOR = Color.black

    attr_reader :width, :height

    def initialize(width, height)
      @width = width
      @height = height
      @pixels = initial_pixels(width, height)
    end

    def pixel_at(x, y)
      @pixels[y][x]
    end

    def write(x, y, color)
      @pixels[y][x] = color
    rescue
      raise "Error writing pixel at (#{x}, #{y})"
    end

    private

    def initial_pixels(width, height)
      height.times.map do
        width.times.map { INITIAL_COLOR }
      end
    end
  end
end
