require_relative 'matrix'
require_relative 'transformations/transformation'

module Raytracer
  class Transformations
    def self.do
      Transformation.new
    end

    def self.translation(x, y, z)
      Raytracer::Matrix.from_array([
        [1, 0, 0, x],
        [0, 1, 0, y],
        [0, 0, 1, z],
        [0, 0, 0, 1]
      ])
    end

    def self.scaling(x, y, z)
      Raytracer::Matrix.from_array([
        [x, 0, 0, 0],
        [0, y, 0, 0],
        [0, 0, z, 0],
        [0, 0, 0, 1]
      ])
    end

    def self.rotation_x(radians)
      Raytracer::Matrix.from_array([
        [1, 0, 0, 0],
        [0, Math.cos(radians), -Math.sin(radians), 0],
        [0, Math.sin(radians), Math.cos(radians), 0],
        [0, 0, 0, 1]
      ])
    end

    def self.rotation_y(radians)
      Raytracer::Matrix.from_array([
        [Math.cos(radians), 0, Math.sin(radians), 0],
        [0, 1, 0, 0],
        [-Math.sin(radians), 0, Math.cos(radians), 0],
        [0, 0, 0, 1]
      ])
    end

    def self.rotation_z(radians)
      Raytracer::Matrix.from_array([
        [Math.cos(radians), -Math.sin(radians), 0, 0],
        [Math.sin(radians), Math.cos(radians), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
      ])
    end

    def self.shearing(xy, xz, yx, yz, zx, zy)
      Raytracer::Matrix.from_array([
        [1, xy, xz, 0],
        [yx, 1, yz, 0],
        [zx, zy, 1, 0],
        [0, 0, 0, 1]
      ])
    end

    private

    def self.identity
      Raytracer::Matrix.from_array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
      ])
    end
  end
end
