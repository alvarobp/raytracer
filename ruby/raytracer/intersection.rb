module Raytracer
  class Intersection
    attr_reader :position, :object

    def initialize(position, object)
      @position = position
      @object = object
    end
  end
end
