require 'matrix'

module Raytracer
  class Matrix
    FLOAT_PRECISION = 5

    def self.from_array(array)
      new(array)
    end

    def initialize(array)
      @delegate = ::Matrix[*array]
    end

    def at(row, column)
      @delegate[row, column]
    end

    def ==(other)
      @delegate == other.delegate
    end

    def *(other)
      array =(@delegate * other.delegate).to_a
      self.class.from_array(array)
    end

    def by_tuple(tuple)
      product = @delegate * tuple_to_vector_matrix(tuple)
      vector_matrix_to_tuple(product)
    end

    def identity
      array = ::Matrix.identity(@delegate.row_count).to_a
      self.class.from_array(array)
    end

    def transpose
      array = @delegate.transpose.to_a
      self.class.from_array(array)
    end

    def determinant
      @delegate.determinant
    end

    def submatrix(row, column)
      array = @delegate.first_minor(row, column).to_a
      self.class.from_array(array)
    end

    def minor(row, column)
      submatrix(row, column).determinant
    end

    def cofactor(row, column)
      @delegate.cofactor(row, column)
    end

    def invertible?
      determinant != 0
    end

    def inverse
      array = rationals_to_floats(@delegate.inverse.to_a)
      self.class.from_array(array)
    end

    protected

    attr_reader :delegate

    private

    def tuple_to_vector_matrix(tuple)
      ::Matrix.column_vector([tuple.x, tuple.y, tuple.z, tuple.w])
    end

    def vector_matrix_to_tuple(matrix)
      components = matrix.to_a.flatten
      Tuple.new(*components)
    end

    def rationals_to_floats(array)
      array.map do |row|
        row.map { |rational| rational.to_f.round(FLOAT_PRECISION) }
      end
    end
  end
end
