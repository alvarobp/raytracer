require_relative 'tuple'
require_relative 'matrix'
require_relative 'intersection'
require_relative 'intersections'

module Raytracer
  class Sphere
    attr_reader :transformation

    def initialize
      @origin = Tuple.point(0, 0, 0)
      @transformation = no_transformation
    end

    def intersect(a_ray)
      ray = a_ray.transform(transformation.inverse)
      sphere_to_ray = ray.origin - @origin

      a = ray.direction.dot(ray.direction)
      b = 2 * ray.direction.dot(sphere_to_ray)
      c = sphere_to_ray.dot(sphere_to_ray) - 1

      discriminant = (b*b) - (4 * a * c)

      if discriminant < 0
        return Intersections.none
      end

      first_position = (-b - Math.sqrt(discriminant)) / (2 * a)
      second_position = (-b + Math.sqrt(discriminant)) / (2 * a)

      Intersections.new(
        intersection_at(first_position),
        intersection_at(second_position)
      )
    end

    def transform(transformation)
      @transformation = transformation
    end

    private

    def intersection_at(position)
      Intersection.new(position, self)
    end

    def no_transformation
      identity = Raytracer::Matrix.from_array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
      ])
      identity
    end
  end
end
