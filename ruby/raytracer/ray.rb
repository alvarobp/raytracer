module Raytracer
  class Ray
    attr_reader :origin, :direction

    def initialize(origin, direction)
      @origin = origin
      @direction = direction
    end

    def position(distance)
      @origin + (@direction * distance)
    end

    def transform(transformation)
      self.class.new(
        transformation.by_tuple(@origin),
        transformation.by_tuple(@direction)
      )
    end
  end
end
