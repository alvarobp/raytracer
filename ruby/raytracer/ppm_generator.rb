module Raytracer
  class PpmGenerator
    MAX_COLOR = 255

    def generate(canvas)
      header = "P3\n#{canvas.width} #{canvas.height}\n#{MAX_COLOR}"
      header + "\n" + pixel_data(canvas) + "\n"
    end

    private

    def pixel_data(canvas)
      (0..canvas.height-1).map do |y|
        pixel_lines_for(y, canvas).join("\n")
      end.join("\n")
    end

    def pixel_lines_for(y, canvas)
      lines = []
      current_line = ''
      color_values = (0..canvas.width-1).map do |x|
        color = canvas.pixel_at(x, y)

        [
          color_value(color.red),
          color_value(color.green),
          color_value(color.blue)
        ]
      end.flatten
      color_values.each do |value|
        string = if current_line.empty?
          value.to_s
        else
          ' ' + value.to_s
        end
        if (current_line + string).size <= 70
          current_line += string
        else
          lines << current_line
          current_line = value.to_s
        end
      end
      lines << current_line
      lines
    end

    def color_value(float)
      value = (float * MAX_COLOR).round.to_i
      return MAX_COLOR if value >= MAX_COLOR
      return 0 if value <= 0

      value
    end
  end
end
