module Raytracer
  class FloatComparator
    EPSILON = 0.00002

    def self.equal?(a_float, another_float)
      (a_float - another_float).abs < EPSILON
    end
  end
end
