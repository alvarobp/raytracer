module Raytracer
  class Intersections
    def self.none
      new(*[])
    end

    def initialize(*intersections)
      @intersections = intersections
    end

    def count
      @intersections.count
    end

    def [](index)
      @intersections[index]
    end

    def empty?
      @intersections.empty?
    end

    def hit
      ordered(positive(@intersections)).first
    end

    private

    def positive(intersections)
      intersections.reject { |intersection| intersection.position < 0 }
    end

    def ordered(intersections)
      intersections.sort { |a, b| a.position <=> b.position }
    end
  end
end
