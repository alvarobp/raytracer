require_relative 'float_comparator'

module Raytracer
  class Color
    attr_reader :red, :green, :blue

    def self.black
      new(0.0, 0.0, 0.0)
    end

    def initialize(red, green, blue)
      @red = red
      @green = green
      @blue = blue
    end

    def +(other)
      Color.new(
        @red + other.red,
        @green + other.green,
        @blue + other.blue
      )
    end

    def -(other)
      Color.new(
        @red - other.red,
        @green - other.green,
        @blue - other.blue
      )
    end

    def *(scalar)
      Color.new(
        @red * scalar,
        @green * scalar,
        @blue * scalar
      )
    end

    def product(other)
      Color.new(
        @red * other.red,
        @green * other.green,
        @blue * other.blue
      )
    end

    def ==(other)
      equal_float?(@red, other.red) &&
      equal_float?(@green, other.green) &&
      equal_float?(@blue, other.blue)
    end

    private

    def equal_float?(a_float, another_float)
      FloatComparator.equal?(a_float, another_float)
    end
  end
end
