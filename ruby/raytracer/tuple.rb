module Raytracer
  require_relative 'float_comparator'

  class Tuple
    W_POINT = 1.0
    W_VECTOR = 0.0

    attr_reader :x, :y, :z, :w

    def self.point(x, y, z)
      new(x, y, z, W_POINT)
    end

    def self.vector(x, y, z)
      new(x, y, z, W_VECTOR)
    end

    def initialize(x, y, z, w)
      @x = x
      @y = y
      @z = z
      @w = w
    end

    def point?
      @w == W_POINT
    end

    def vector?
      @w == W_VECTOR
    end

    def +(other)
      check_addition(other)

      Tuple.new(
        @x + other.x,
        @y + other.y,
        @z + other.z,
        @w + other.w
      )
    end

    def -(other)
      check_subtraction(other)

      x = @x - other.x
      y = @y - other.y
      z = @z - other.z

      if point? && other.vector?
        Tuple.point(x, y, z)
      else
        Tuple.vector(x, y ,z)
      end
    end

    def *(scalar)
      Tuple.new(@x*scalar, @y*scalar, @z*scalar, @w*scalar)
    end

    def /(scalar)
      Tuple.new(@x/scalar, @y/scalar, @z/scalar, @w/scalar)
    end

    def negate
      Tuple.new(-@x, -@y, -@z, -@w)
    end

    def magnitude
      Math.sqrt(@x*@x + @y*@y + @z*@z + @w*@w)
    end

    def normalize
      self.class.new(
        @x / magnitude,
        @y / magnitude,
        @z / magnitude,
        @w / magnitude
      )
    end

    def dot(other)
      @x * other.x +
      @y * other.y +
      @z * other.z +
      @w * other.w
    end

    def cross(other)
      Tuple.vector(
        @y * other.z - @z * other.y,
        @z * other.x - @x * other.z,
        @x * other.y - @y * other.x
      )
    end

    def ==(other)
      equal_float?(@x, other.x) &&
      equal_float?(@y, other.y) &&
      equal_float?(@z, other.z) &&
      equal_float?(@w, other.w)
    end

    def to_s
      "(#{@x}, #{@y}, #{@z}, #{@w})"
    end

    private

    def check_addition(other)
      return unless point? && other.point?

      raise ArgumentError.new('Cannot add a point to another')
    end

    def check_subtraction(other)
      return unless vector? && other.point?

      raise ArgumentError.new('Cannot subtract a point from a vector')
    end

    def equal_float?(a_float, another_float)
      FloatComparator.equal?(a_float, another_float)
    end
  end
end
