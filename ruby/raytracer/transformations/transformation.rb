module Raytracer
  class Transformations
    class Transformation
      def initialize
        @matrix = Transformations.identity
      end

      def translation(x, y, z)
        @matrix = Transformations.translation(x, y, z) * @matrix
        self
      end

      def scaling(x, y, z)
        @matrix = Transformations.scaling(x, y, z) * @matrix
        self
      end

      def rotation_x(radians)
        @matrix = Transformations.rotation_x(radians) * @matrix
        self
      end

      def rotation_y(radians)
        @matrix = Transformations.rotation_y(radians) * @matrix
        self
      end

      def rotation_z(radians)
        @matrix = Transformations.rotation_z(radians) * @matrix
        self
      end

      def shearing(xy, xz, yx, yz, zx, zy)
        @matrix = Transformations.shearing(xy, xz, yx, yz, zx, zy) * @matrix
        self
      end

      def by_tuple(tuple)
        @matrix.by_tuple(tuple)
      end
    end

    private_constant :Transformation
  end
end
