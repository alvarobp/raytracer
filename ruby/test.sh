#!/bin/bash

docker build . --quiet --tag alvarobp/raytracer-ruby
docker run --rm --tty --volume $(pwd):/opt/code alvarobp/raytracer-ruby rspec
