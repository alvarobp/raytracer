## Requirements

- Docker: https://docs.docker.com/install/

## Usage

### Run tests

```
bash test.sh
```

### Run exercises

```
bash exercises/chapterN/run.sh
```
