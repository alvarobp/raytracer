require 'fileutils'

require_relative '../../raytracer/tuple'
require_relative '../../raytracer/canvas'
require_relative '../../raytracer/color'
require_relative '../../raytracer/ppm_generator'

class Projectile
  attr_accessor :position, :velocity

  def initialize(position, velocity)
    @position = position
    @velocity = velocity
  end
end

class Environment
  attr_accessor :gravity, :wind

  def initialize(gravity, wind)
    @gravity = gravity
    @wind = wind
  end
end

def tick(environment, projectile)
  position = projectile.position + projectile.velocity
  velocity = projectile.velocity + environment.gravity + environment.wind

  Projectile.new(position, velocity)
end

def save_image(canvas)
  ppm_generator = Raytracer::PpmGenerator.new
  output_directory = File.join(File.dirname(__FILE__), 'output')
  file_path = File.join(
    output_directory,
    'raytracer-ruby-chapter2.ppm'
  )
  ppm = ppm_generator.generate(canvas)
  FileUtils.mkdir_p(output_directory)
  File.write(file_path, ppm)
  puts "Generated PPM file: #{file_path}"
end

def simulate
  projectile = Projectile.new(
    Raytracer::Tuple.point(0.0, 1.0, 0.0),
    Raytracer::Tuple.vector(1.0, 1.8, 0.0).normalize * 11.25
  )
  environment = Environment.new(
    Raytracer::Tuple.vector(0.0, -0.1, 0.0),
    Raytracer::Tuple.vector(-0.01, 0.0, 0.0)
  )
  canvas = Raytracer::Canvas.new(900, 550)
  color = Raytracer::Color.new(1.0, 0.0, 0.0)

  ticks = 1

  while projectile.position.y > 0.0
    canvas.write(
      projectile.position.x.round,
      canvas.height - projectile.position.y.round,
      color
    )
    projectile = tick(environment, projectile)
    ticks += 1
  end

  save_image(canvas)
end

simulate
