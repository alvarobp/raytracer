require 'fileutils'

require_relative '../../raytracer/tuple'
require_relative '../../raytracer/canvas'
require_relative '../../raytracer/color'
require_relative '../../raytracer/transformations'
require_relative '../../raytracer/ppm_generator'

def save_image(canvas)
  ppm_generator = Raytracer::PpmGenerator.new
  output_directory = File.join(File.dirname(__FILE__), 'output')
  file_path = File.join(
    output_directory,
    'raytracer-ruby-chapter4.ppm'
  )
  ppm = ppm_generator.generate(canvas)
  FileUtils.mkdir_p(output_directory)
  File.write(file_path, ppm)
  puts "Generated PPM file: #{file_path}"
end

def draw_in_canvas(canvas, point, color)
  canvas.write(
    point.x + canvas.width/2,
    point.y - canvas.height/2,
    color
  )
end

def draw_clock
  canvas = Raytracer::Canvas.new(300, 300)
  color = Raytracer::Color.new(1.0, 1.0, 1.0)
  start = Raytracer::Tuple.point(0.0, -100.0, 0.0)
  rotation_increment = (2*Math::PI) / 12

  draw_in_canvas(canvas, start, color)

  point = start

  11.times do |time|
    point = Raytracer::Transformations.
      rotation_z(rotation_increment).
      by_tuple(point)
    draw_in_canvas(canvas, point, color)
  end

  canvas
end

save_image(draw_clock)
