require_relative '../../raytracer/tuple'

class Projectile
  attr_accessor :position, :velocity

  def initialize(position, velocity)
    @position = position
    @velocity = velocity
  end
end

class Environment
  attr_accessor :gravity, :wind

  def initialize(gravity, wind)
    @gravity = gravity
    @wind = wind
  end
end

def tick(environment, projectile)
  position = projectile.position + projectile.velocity
  velocity = projectile.velocity + environment.gravity + environment.wind

  Projectile.new(position, velocity)
end

def simulate
  projectile = Projectile.new(
    Raytracer::Tuple.point(0.0, 1.0, 0.0),
    Raytracer::Tuple.vector(1.0, 1.0, 0.0).normalize
  )
  environment = Environment.new(
    Raytracer::Tuple.vector(0.0, -0.1, 0.0),
    Raytracer::Tuple.vector(-0.01, 0.0, 0.0)
  )

  ticks = 1

  while projectile.position.y > 0.0
    puts projectile.position
    projectile = tick(environment, projectile)
    ticks += 1
  end

  puts "Hit the ground after #{ticks} ticks"
end

simulate
