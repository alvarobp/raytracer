require 'spec_helper'

require_relative '../raytracer/ray'

describe 'A Ray' do
  it 'computes a point from a distance' do
    origin = a_point(2, 3, 4)
    direction = a_vector(1, 0, 0)
    ray = Raytracer::Ray.new(origin, direction)

    expect(ray.position(0)).to eq(a_point(2, 3, 4))
    expect(ray.position(1)).to eq(a_point(3, 3, 4))
    expect(ray.position(-1)).to eq(a_point(1, 3, 4))
    expect(ray.position(2.5)).to eq(a_point(4.5, 3, 4))
  end

  it 'can be translated' do
    origin = a_point(1, 2, 3)
    direction = a_vector(0, 1, 0)
    ray = Raytracer::Ray.new(origin, direction)
    translation = Raytracer::Transformations.translation(3, 4, 5)

    result = ray.transform(translation)

    expect(result.origin).to eq(a_point(4, 6, 8))
    expect(result.direction).to eq(a_vector(0, 1, 0))
  end

  it 'can be scaled' do
    origin = a_point(1, 2, 3)
    direction = a_vector(0, 1, 0)
    ray = Raytracer::Ray.new(origin, direction)
    scaling = Raytracer::Transformations.scaling(2, 3, 4)

    result = ray.transform(scaling)

    expect(result.origin).to eq(a_point(2, 6, 12))
    expect(result.direction).to eq(a_vector(0, 3, 0))
  end
end
