module FactoryHelpers
  def a_canvas(width = 2, height = 2)
    Raytracer::Canvas.new(width, height)
  end

  def a_color(red, green, blue)
    Raytracer::Color.new(red, green, blue)
  end

  def a_tuple(x, y, z, w)
    Raytracer::Tuple.new(x, y, z, w)
  end

  def a_point(x, y, z)
    Raytracer::Tuple.point(x, y, z)
  end

  def a_vector(x, y, z)
    Raytracer::Tuple.vector(x, y, z)
  end

  def a_sphere
    Raytracer::Sphere.new
  end
end
