require_relative 'support/factory_helpers'

RSpec.configure do |config|
  config.filter_run_when_matching :focus
  config.include FactoryHelpers
end
