require 'spec_helper'

require_relative '../raytracer/transformations'

describe 'Transformations' do
  it 'provides translation of a point' do
    transformation = Raytracer::Transformations.translation(5, -3, 2)
    point = a_point(-3, 4, 5)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(2, 1, 7))
  end

  it 'provides an invertible translation' do
    transformation = Raytracer::Transformations.translation(5, -3, 2)
    inverse = transformation.inverse
    point = a_point(-3, 4, 5)

    result = inverse.by_tuple(point)

    expect(result).to eq(a_point(-8, 7, 3))
  end

  it 'provides translations that do not affect vectors' do
    transformation = Raytracer::Transformations.translation(5, -3, 2)
    vector = a_vector(-3, 4, 5)

    result = transformation.by_tuple(vector)

    expect(result).to eq(vector)
  end

  it 'provides scaling of a point' do
    transformation = Raytracer::Transformations.scaling(2, 3, 4)
    point = a_point(-4, 6, 8)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(-8, 18, 32))
  end

  it 'provides scaling of a vector' do
    transformation = Raytracer::Transformations.scaling(2, 3, 4)
    vector = a_vector(-4, 6, 8)

    result = transformation.by_tuple(vector)

    expect(result).to eq(a_vector(-8, 18, 32))
  end

  it 'provides an invertible scaling' do
    transformation = Raytracer::Transformations.scaling(2, 3, 4)
    inverse = transformation.inverse
    vector = a_vector(-4, 6, 8)

    result = inverse.by_tuple(vector)

    expect(result).to eq(a_vector(-2, 2, 2))
  end

  it 'provides reflection by scaling by a negative value' do
    transformation = Raytracer::Transformations.scaling(-1, 1, 1)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(-2, 3, 4))
  end

  it 'provides rotation around the x axis' do
    point = a_point(0, 1, 0)
    half_quarter_rotation = Raytracer::Transformations.rotation_x(pi/4)
    full_quarter_rotation = Raytracer::Transformations.rotation_x(pi/2)

    expect(half_quarter_rotation.by_tuple(point)).to eq(
      a_point(0, Math.sqrt(2)/2, Math.sqrt(2)/2)
    )
    expect(full_quarter_rotation.by_tuple(point)).to eq(
      a_point(0, 0, 1)
    )
  end

  it 'provides reverse rotation around the x axis' do
    point = a_point(0, 1, 0)
    half_quarter_rotation = Raytracer::Transformations.rotation_x(pi/4)
    inverse = half_quarter_rotation.inverse

    expect(inverse.by_tuple(point)).to eq(
      a_point(0, Math.sqrt(2)/2, -Math.sqrt(2)/2)
    )
  end

  it 'provides rotation around the x axis' do
    point = a_point(0, 0, 1)
    half_quarter_rotation = Raytracer::Transformations.rotation_y(pi/4)
    full_quarter_rotation = Raytracer::Transformations.rotation_y(pi/2)

    expect(half_quarter_rotation.by_tuple(point)).to eq(
      a_point(Math.sqrt(2)/2, 0, Math.sqrt(2)/2)
    )
    expect(full_quarter_rotation.by_tuple(point)).to eq(
      a_point(1, 0, 0)
    )
  end

  it 'provides rotation around the z axis' do
    point = a_point(0, 1, 0)
    half_quarter_rotation = Raytracer::Transformations.rotation_z(pi/4)
    full_quarter_rotation = Raytracer::Transformations.rotation_z(pi/2)

    expect(half_quarter_rotation.by_tuple(point)).to eq(
      a_point(-Math.sqrt(2)/2, Math.sqrt(2)/2, 0)
    )
    expect(full_quarter_rotation.by_tuple(point)).to eq(
      a_point(-1, 0, 0)
    )
  end

  it 'provides shearing moving x in proportion to y' do
    transformation = Raytracer::Transformations.shearing(1, 0, 0, 0, 0, 0)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(5, 3, 4))
  end

  it 'provides shearing moving x in proportion to z' do
    transformation = Raytracer::Transformations.shearing(0, 1, 0, 0, 0, 0)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(6, 3, 4))
  end

  it 'provides shearing moving y in proportion to x' do
    transformation = Raytracer::Transformations.shearing(0, 0, 1, 0, 0, 0)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(2, 5, 4))
  end

  it 'provides shearing moving y in proportion to z' do
    transformation = Raytracer::Transformations.shearing(0, 0, 0, 1, 0, 0)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(2, 7, 4))
  end

  it 'provides shearing moving z in proportion to x' do
    transformation = Raytracer::Transformations.shearing(0, 0, 0, 0, 1, 0)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(2, 3, 6))
  end

  it 'provides shearing moving z in proportion to y' do
    transformation = Raytracer::Transformations.shearing(0, 0, 0, 0, 0, 1)
    point = a_point(2, 3, 4)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(2, 3, 7))
  end

  it 'can apply multiple transformations individually in sequence' do
    point = a_point(1, 0, 1)
    first = Raytracer::Transformations.rotation_x(pi/2)
    second = Raytracer::Transformations.scaling(5, 5, 5)
    third = Raytracer::Transformations.translation(10, 5, 7)

    result = first.by_tuple(point)
    expect(result).to eq(a_point(1, -1, 0))

    result = second.by_tuple(result)
    expect(result).to eq(a_point(5, -5, 0))

    result = third.by_tuple(result)
    expect(result).to eq(a_point(15, 0, 7))
  end

  it 'can apply chained transformations in reverse order' do
    point = a_point(1, 0, 1)
    first = Raytracer::Transformations.rotation_x(pi/2)
    second = Raytracer::Transformations.scaling(5, 5, 5)
    third = Raytracer::Transformations.translation(10, 5, 7)
    chained = third * second * first

    result = chained.by_tuple(point)

    expect(result).to eq(a_point(15, 0, 7))
  end

  it 'builds up a set of chained transformations as a single unit' do
    point = a_point(1, 0, 1)
    transformation = Raytracer::Transformations.do.
      rotation_x(pi/2).
      scaling(5, 5, 5).
      translation(10, 5, 7)

    result = transformation.by_tuple(point)

    expect(result).to eq(a_point(15, 0, 7))
  end

  def pi
    Math::PI
  end
end
