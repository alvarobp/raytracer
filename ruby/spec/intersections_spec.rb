require 'spec_helper'

require_relative '../raytracer/sphere'
require_relative '../raytracer/intersections'

describe 'An Intersections collection' do
  it 'aggregates multiple intersections' do
    sphere = a_sphere
    intersection = an_intersection(1, sphere)
    another_intersection = an_intersection(2, sphere)

    intersections = Raytracer::Intersections.new(
      intersection,
      another_intersection
    )

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(1)
    expect(intersections[1].position).to eq(2)
  end

  it 'returns the hit when all intersections have positive position' do
    sphere = a_sphere
    intersection = an_intersection(1, sphere)
    intersections = Raytracer::Intersections.new(
      an_intersection(2, sphere),
      intersection
    )

    hit = intersections.hit

    expect(hit).to eq(intersection)
  end

  it 'returns the hit when some intersections have negative position' do
    sphere = a_sphere
    intersection = an_intersection(1, sphere)
    intersections = Raytracer::Intersections.new(
      intersection,
      an_intersection(-1, sphere)
    )

    hit = intersections.hit

    expect(hit).to eq(intersection)
  end

  it 'returns no hit when all intersections have negative position' do
    sphere = a_sphere
    intersections = Raytracer::Intersections.new(
      an_intersection(-2, sphere),
      an_intersection(-1, sphere)
    )

    hit = intersections.hit

    expect(hit).to eq(nil)
  end

  it 'returns the lowest non-negative intersection as the hit' do
    sphere = a_sphere
    intersection = an_intersection(2, sphere)
    intersections = Raytracer::Intersections.new(
      an_intersection(5, sphere),
      an_intersection(7, sphere),
      an_intersection(-3, sphere),
      intersection
    )

    hit = intersections.hit

    expect(hit).to eq(intersection)
  end

  def an_intersection(position, object)
    Raytracer::Intersection.new(position, object)
  end
end

