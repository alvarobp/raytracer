require 'spec_helper'

require_relative '../raytracer/color'

describe 'A Color' do
  it 'is made of red, green and blue values' do
    color = a_color(-0.5, 0.4, 1.7)

    expect(color.red).to eq(-0.5)
    expect(color.green).to eq(0.4)
    expect(color.blue).to eq(1.7)
  end

  it 'adds to another' do
    a_color = a_color(0.9, 0.6, 0.75)
    another_color = a_color(0.7, 0.1, 0.25)

    result = a_color + another_color

    expect(result).to eq(a_color(1.6, 0.7, 1.0))
  end

  it 'subtracts from another' do
    a_color = a_color(0.9, 0.6, 0.75)
    another_color = a_color(0.7, 0.1, 0.25)

    result = a_color - another_color

    expect(result).to eq(a_color(0.2, 0.5, 0.5))
  end

  it 'multiplies by a scalar' do
    color = a_color(0.2, 0.3, 0.4)

    result = color * 2

    expect(result).to eq(a_color(0.4, 0.6, 0.8))
  end

  it 'multiplies by another color' do
    a_color = a_color(1.0, 0.2, 0.4)
    another_color = a_color(0.9, 1.0, 0.1)

    result = a_color.product(another_color)

    expect(result).to eq(a_color(0.9, 0.2, 0.04))
  end
end
