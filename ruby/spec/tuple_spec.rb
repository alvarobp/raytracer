require 'spec_helper'

require_relative '../raytracer/tuple'

describe 'A Tuple' do
  context 'with w=1.0' do
    let(:tuple) { a_tuple(4.3, 4.2, 3.1, 1.0) }

    it 'is a point' do
      expect(tuple.point?).to be true
    end

    it 'is not a vector' do
      expect(tuple.vector?).to be false
    end
  end

  context 'with w=0' do
    let(:tuple) { a_tuple(4.3, 4.2, 3.1, 0.0) }

    it 'is a vector' do
      expect(tuple.vector?).to be true
    end

    it 'is not a point' do
      expect(tuple.point?).to be false
    end
  end

  it 'creates points' do
    x = 4.0
    y = -4.0
    z = 3.0
    point = a_point(x, y, z)

    expect(point).to eq(a_tuple(x, y, z, 1.0))
  end

  it 'creates vectors' do
    x = 4.0
    y = -4.0
    z = 3.0
    point = a_vector(x, y, z)

    expect(point).to eq(a_tuple(x, y, z, 0.0))
  end

  it 'knows when it is equal to another' do
    x = 4.0
    y = 3.0
    z = 2.0
    w = 0.0
    tuple = a_tuple(x, y, z, w)

    expect(tuple).to eq(a_tuple(x, y, z, w))
    expect(tuple).not_to eq(a_tuple(-x, y, z, w))
    expect(tuple).not_to eq(a_tuple(x, -y, z, w))
    expect(tuple).not_to eq(a_tuple(x, y, -z, w))
    expect(tuple).not_to eq(a_tuple(x, y, z, 1.0))
  end

  it 'can be added to another' do
    a_tuple = a_tuple(3.0, -2.0, 5.0, 1.0)
    another_tuple = a_tuple(-2.0, 3.0, 1.0, 0.0)

    addition = a_tuple + another_tuple

    expect(addition).to eq(a_tuple(1.0, 1.0, 6.0, 1.0))
  end

  it 'cannot add a point to another' do
    a_point = a_point(some_x, some_y, some_z)
    another_point = a_point(some_x, some_y, some_z)

    expect {
      a_point + another_point
    }.to raise_error(ArgumentError, "Cannot add a point to another")
  end

  it 'subtracts two points' do
    a_point = a_point(3.0, 2.0, 1.0)
    another_point = a_point(5.0, 6.0, 7.0)

    result = a_point - another_point

    expect(result).to eq(a_vector(-2.0, -4.0, -6.0))
  end

  it 'subtracts a vector from a point' do
    a_point = a_point(3.0, 2.0, 1.0)
    a_vector = a_vector(5.0, 6.0, 7.0)

    result = a_point - a_vector

    expect(result).to eq(a_point(-2.0, -4.0, -6.0))
  end

  it 'subtracts two vectors' do
    a_vector = a_vector(3.0, 2.0, 1.0)
    another_vector = a_vector(5.0, 6.0, 7.0)

    result = a_vector - another_vector

    expect(result).to eq(a_vector(-2.0, -4.0, -6.0))
  end

  it 'cannot subtract a point from a vector' do
    a_vector = a_vector(some_x, some_y, some_z)
    a_point = a_point(some_x, some_y, some_z)

    expect {
      a_vector - a_point
    }.to raise_error(ArgumentError, "Cannot subtract a point from a vector")
  end

  it 'negates itself' do
    x = 1.0
    y = -2.0
    z = 3.0
    w = -4.0
    a_tuple = a_tuple(x, y, z, w)

    result = a_tuple.negate

    expect(result).to eq(a_tuple(-x, -y, -z, -w))
  end

  it 'multiples by a scalar' do
    tuple = a_tuple(1.0, -2.0, 3.0, -4.0)
    scalar = 3.5

    result = tuple * scalar

    expect(result).to eq(a_tuple(3.5, -7, 10.5, -14))
  end

  it 'multiples by a fraction' do
    tuple = a_tuple(1.0, -2.0, 3.0, -4.0)
    fraction = 0.5

    result = tuple * fraction

    expect(result).to eq(a_tuple(0.5, -1, 1.5, -2))
  end

  it 'divides by a scalar' do
    tuple = a_tuple(1.0, -2.0, 3.0, -4.0)
    scalar = 2.0

    result = tuple / scalar

    expect(result).to eq(a_tuple(0.5, -1, 1.5, -2))
  end

  it 'computes magnitude of vectors' do
    expect(a_vector(1.0, 0.0, 0.0).magnitude).to eq(1.0)
    expect(a_vector(0.0, 1.0, 0.0).magnitude).to eq(1.0)
    expect(a_vector(0.0, 0.0, 1.0).magnitude).to eq(1.0)
    expect(a_vector(1.0, 2.0, 3.0).magnitude).to eq(Math.sqrt(14.0))
    expect(a_vector(-1.0, -2.0, -3.0).magnitude).to eq(Math.sqrt(14.0))
  end

  it 'normalizes vectors' do
    expect(a_vector(4.0, 0.0, 0.0).normalize).to eq(a_vector(1.0, 0.0, 0.0))
    expect(a_vector(1.0, 2.0, 3.0).normalize).to eq(a_vector(0.26726, 0.53452, 0.80178))
  end

  it 'computes the dot product of two vectors' do
    a_vector = a_vector(1.0, 2.0, 3.0)
    another_vector = a_vector(2.0, 3.0, 4.0)

    result = a_vector.dot(another_vector)

    expect(result).to eq(20)
  end

  it 'computes the cross product of two vectors' do
    a_vector = a_vector(1.0, 2.0, 3.0)
    another_vector = a_vector(2.0, 3.0, 4.0)

    expect(a_vector.cross(another_vector)).to eq(a_vector(-1.0, 2.0, -1.0))
    expect(another_vector.cross(a_vector)).to eq(a_vector(1.0, -2.0, 1.0))
  end

  def some_x
    4.1
  end

  def some_y
    3.2
  end

  def some_z
    2.3
  end
end
