require 'spec_helper'

require_relative '../raytracer/sphere'
require_relative '../raytracer/intersection'

describe 'An Intersection' do
  it 'encapsulates position and object' do
    sphere = Raytracer::Sphere.new
    position = 3.5
    intersection = Raytracer::Intersection.new(position, sphere)

    expect(intersection.position).to eq(position)
    expect(intersection.object).to eq(sphere)
  end
end
