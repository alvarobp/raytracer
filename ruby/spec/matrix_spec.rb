require 'spec_helper'

require_relative '../raytracer/matrix'
require_relative '../raytracer/tuple'

describe 'A Matrix' do
  it 'constructs from a 4x4 array' do
    matrix = a_matrix([
      [1, 2, 3, 4],
      [5.5, 6.5, 7.5, 8.5],
      [9, 10, 11, 12],
      [13.5, 14.5, 15.5, 16.5]
    ])

    expect(matrix.at(0, 0)).to eq(1)
    expect(matrix.at(0, 3)).to eq(4)
    expect(matrix.at(1, 0)).to eq(5.5)
    expect(matrix.at(1, 2)).to eq(7.5)
    expect(matrix.at(2, 2)).to eq(11)
    expect(matrix.at(3, 0)).to eq(13.5)
    expect(matrix.at(3, 2)).to eq(15.5)
  end

  it 'constructs from a 2x2 array' do
    matrix = a_matrix([
      [-3, 5],
      [1, -2]
    ])

    expect(matrix.at(0, 0)).to eq(-3)
    expect(matrix.at(0, 1)).to eq(5)
    expect(matrix.at(1, 0)).to eq(1)
    expect(matrix.at(1, 1)).to eq(-2)
  end

  it 'constructs from a 3x3 array' do
    matrix = a_matrix([
      [-3, 5, 0],
      [1, -2, 7],
      [0, 1, 1]
    ])

    expect(matrix.at(0, 0)).to eq(-3)
    expect(matrix.at(1, 1)).to eq(-2)
    expect(matrix.at(2, 2)).to eq(1)
  end

  it 'is equal to another matrix with same values' do
    a_matrix = a_matrix([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 8, 7, 6],
      [5, 4, 3, 2]
    ])
    another_matrix = a_matrix([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 8, 7, 6],
      [5, 4, 3, 2]
    ])

    expect(a_matrix).to eq(another_matrix)
  end

  it 'is not equal to another matrix with different values' do
    a_matrix = a_matrix([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 8, 7, 6],
      [5, 4, 3, 2]
    ])
    another_matrix = a_matrix([
      [2, 3, 4, 5],
      [6, 7, 8, 9],
      [8, 7, 6, 5],
      [4, 3, 2, 1]
    ])

    expect(a_matrix).not_to eq(another_matrix)
  end


  it 'multiplies by another matrix' do
    a_matrix = a_matrix([
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 8, 7, 6],
      [5, 4, 3, 2]
    ])
    another_matrix = a_matrix([
      [-2, 1, 2, 3],
      [3, 2, 1, -1],
      [4, 3, 6, 5],
      [1, 2, 7, 8]
    ])

    result = a_matrix * another_matrix

    expect(result).to eq(
      a_matrix([
        [20, 22, 50, 48],
        [44, 54, 114, 108],
        [40, 58, 110, 102],
        [16, 26, 46, 42]
      ])
    )
  end

  it 'multiplies by a tuple' do
    matrix = a_matrix([
      [1, 2, 3, 4],
      [2, 4, 4, 2],
      [8, 6, 4, 1],
      [0, 0, 0, 1]
    ])
    tuple = a_tuple(1, 2, 3, 1)

    result = matrix.by_tuple(tuple)

    expect(result).to eq(a_tuple(18, 24, 33, 1))
  end

  it 'provides its identity matrix' do
    matrix = a_matrix([
      [0, 1, 2, 4],
      [1, 2, 4, 8],
      [2, 4, 8, 16],
      [4, 8, 16, 32]
    ])

    identity = matrix.identity

    expect(identity).to eq(
      a_matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
      ])
    )
  end

  it 'stays the same when multiplied by its identity' do
    matrix = a_matrix([
      [0, 1, 2, 4],
      [1, 2, 4, 8],
      [2, 4, 8, 16],
      [4, 8, 16, 32]
    ])

    result = matrix * matrix.identity

    expect(result).to eq(matrix)
  end

  it 'multiplies identity by a tuple' do
    matrix = a_matrix([
      [0, 1, 2, 4],
      [1, 2, 4, 8],
      [2, 4, 8, 16],
      [4, 8, 16, 32]
    ])
    identity = matrix.identity
    tuple = a_tuple(1, 2, 3, 4)

    result = identity.by_tuple(tuple)

    expect(result).to eq(tuple)
  end

  it 'provides its transpose' do
    matrix = a_matrix([
      [0, 9, 3, 0],
      [9, 8, 0, 8],
      [1, 8, 5, 3],
      [0, 0, 5, 8]
    ])

    result = matrix.transpose

    expect(result).to eq(
      a_matrix([
        [0, 9, 1, 0],
        [9, 8, 8, 0],
        [3, 0, 5, 5],
        [0, 8, 3, 8]
      ])
    )
  end

  it 'transposes a matrix identity to itself' do
    matrix = an_identity_matrix

    result = matrix.transpose

    expect(result).to eq(matrix)
  end

  it 'calculates its determinant' do
    matrix = a_matrix([
      [1, 5],
      [-3, 2]
    ])

    result = matrix.determinant

    expect(result).to eq(17)
  end

  it 'provides submatrix from a 3x3 matrix' do
    matrix = a_matrix([
      [1, 5, 0],
      [-3, 2, 7],
      [0, 6, -3]
    ])

    result = matrix.submatrix(0, 2)

    expect(result).to eq(
      a_matrix([
        [-3, 2],
        [0, 6]
      ])
    )
  end

  it 'provides submatrix from a 4x4 matrix' do
    matrix = a_matrix([
      [-6, 1, 1, 6],
      [-8, 5, 8, 6],
      [-1, 0, 8, 2],
      [-7, 1, -1, 1]
    ])

    result = matrix.submatrix(2, 1)

    expect(result).to eq(
      a_matrix([
        [-6, 1, 6],
        [-8, 8, 6],
        [-7, -1, 1]
      ])
    )
  end

  it 'calculates minor of a 3x3 matrix' do
    matrix = a_matrix([
      [3, 5, 0],
      [2, -1, -7],
      [6, -1, 5]
    ])

    result = matrix.minor(1, 0)

    expect(result).to eq(25)
  end

  it 'calculates cofactor of a 3x3 matrix' do
    matrix = a_matrix([
      [3, 5, 0],
      [2, -1, -7],
      [6, -1, 5]
    ])

    expect(matrix.cofactor(0, 0)).to eq(-12)
    expect(matrix.cofactor(1, 0)).to eq(-25)
  end

  it 'calculates cofactor of a 4x4 matrix' do
    matrix = a_matrix([
      [-2, -8, 3, 5],
      [-3, 1, 7, 3],
      [1, 2, -9, 6],
      [-6, 7, 7, -9]
    ])

    expect(matrix.cofactor(0, 0)).to eq(690)
    expect(matrix.cofactor(0, 1)).to eq(447)
    expect(matrix.cofactor(0, 2)).to eq(210)
    expect(matrix.cofactor(0, 3)).to eq(51)
  end

  it 'calculates determinant of a 3x3 matrix' do
    matrix = a_matrix([
      [1, 2, 6],
      [-5, 8, -4],
      [2, 6, 4]
    ])

    determinant = matrix.determinant

    expect(determinant).to eq(-196)
  end

  it 'calculates determinant of a 4x4 matrix' do
    matrix = a_matrix([
      [-2, -8, 3, 5],
      [-3, 1, 7, 3],
      [1, 2, -9, 6],
      [-6, 7, 7, -9]
    ])

    determinant = matrix.determinant

    expect(determinant).to eq(-4071)
  end

  it 'tells when it is invertible' do
    matrix = a_matrix([
      [6, 4, 4, 4],
      [5, 5, 7, 6],
      [4, -9, 3, -7],
      [9, 1, 7, -6]
    ])

    expect(matrix).to be_invertible
  end

  it 'tells when it is not invertible' do
    matrix = a_matrix([
      [-4, 2, -2, -3],
      [9, 6, 2, 6],
      [0, -5, 1, -5],
      [0, 0, 0, 0]
    ])

    expect(matrix).not_to be_invertible
  end

  it 'calculates its inverse' do
    matrix_a = a_matrix([
      [-5, 2, 6, -8],
      [1, -5, 1, 8],
      [7, 7, -6, -7],
      [1, -3, 7, 4]
    ])
    matrix_b = a_matrix([
      [8, -5, 9, 2],
      [7, 5, 6, 1],
      [-6, 0, 9, 6],
      [-3, 0, -9, -4]
    ])
    matrix_c = a_matrix([
      [9, 3, 0, 9],
      [-5, -2, -6, -3],
      [-4, 9, 6, 4],
      [-7, 6, 6, 2]
    ])

    inverse_of_a = matrix_a.inverse
    inverse_of_b = matrix_b.inverse
    inverse_of_c = matrix_c.inverse

    expect(inverse_of_a).to eq(
      a_matrix([
        [0.21805, 0.45113, 0.24060, -0.04511],
        [-0.80827, -1.45677, -0.44361, 0.52068],
        [-0.07895, -0.22368, -0.05263, 0.19737],
        [-0.52256, -0.81391, -0.30075, 0.30639]
      ])
    )
    expect(inverse_of_b).to eq(
      a_matrix([
        [-0.15385, -0.15385, -0.28205, -0.53846],
        [-0.07692, 0.12308, 0.02564, 0.03077],
        [0.35897, 0.35897, 0.43590, 0.92308],
        [-0.69231, -0.69231, -0.76923, -1.92308]
      ])
    )
    expect(inverse_of_c).to eq(
      a_matrix([
        [-0.04074, -0.07778, 0.14444, -0.22222],
        [-0.07778, 0.03333, 0.36667, -0.33333],
        [-0.02901, -0.14630, -0.10926, 0.12963],
        [0.17778, 0.06667, -0.26667, 0.33333]
      ])
    )
  end

  it 'is itself when multiplying a product by the inverse' do
    skip('passing this means comparing matrices using float comparator which works at a higher precision than the given by inverse')

    multiplier = a_matrix([
      [3, -9, 7, 3],
      [3, -8, 2, -9],
      [-4, 4, 4, 1],
      [-6, 5, -1, 1]
    ])
    multiplicant = a_matrix([
      [8, 2, 2, 2],
      [3, -1, 7, 0],
      [7, 0, 5, 4],
      [6, -2, 0, 5]
    ])
    product = multiplier * multiplicant

    result = product * multiplicant.inverse

    expect(result).to eq(multiplier)
  end

  def a_matrix(array)
    Raytracer::Matrix.from_array(array)
  end

  def an_identity_matrix
    a_matrix([
      [1, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 1, 0],
      [0, 0, 0, 1]
    ])
  end
end
