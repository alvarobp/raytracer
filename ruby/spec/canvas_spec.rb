require 'spec_helper'

require_relative '../raytracer/color'
require_relative '../raytracer/canvas'

describe 'A Canvas' do
  it 'is black at the beginning' do
    width = 10
    height = 20
    canvas = a_canvas(width, height)

    for_all_pixels(width, height) do |x, y|
      pixel = canvas.pixel_at(x, y)
      expect(pixel).to eq(black_color),
        "Expected #{black_color.inspect} at (#{x}, #{y}) got #{pixel.inspect}"
    end
  end

  it 'writes pixels' do
    canvas = a_canvas(10, 20)
    color = a_color(1.0, 0.0, 0.0)

    canvas.write(2.0, 3.0, color)

    expect(canvas.pixel_at(2.0, 3.0)).to eq(color)
  end

  def black_color
    Raytracer::Color.black
  end

  def for_all_pixels(width, height)
    (0..height-1).each do |y|
      (0..width-1).each do |x|
        yield(x, y)
      end
    end
  end
end
