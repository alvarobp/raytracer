require 'spec_helper'

require_relative '../raytracer/canvas'
require_relative '../raytracer/color'
require_relative '../raytracer/ppm_generator'

describe 'A PpmGenerator' do
  it 'indicates ASCII Portable PixMap format in header' do
    ppm_magic_number = 'P3'
    canvas = a_canvas

    result = a_generator.generate(canvas)

    lines = lines_from(result)
    expect(lines[0]).to eq(ppm_magic_number)
  end

  it 'includes dimensions in header' do
    width = 5
    height = 3
    canvas = a_canvas(width, height)

    result = a_generator.generate(canvas)

    lines = lines_from(result)
    expect(lines[1]).to eq("#{width} #{height}")
  end

  it 'includes maximum color in header' do
    maximum_color = 255
    canvas = a_canvas

    result = a_generator.generate(canvas)

    lines = lines_from(result)
    expect(lines[2]).to eq("#{maximum_color}")
  end

  it 'includes pixel data' do
    canvas = a_canvas(5, 3)
    canvas.write(0, 0, a_color(1.5, 0.0, 0.0))
    canvas.write(2, 1, a_color(0.0, 0.5, 0.0))
    canvas.write(4, 2, a_color(-0.5, 0.0, 1.0))

    result = a_generator.generate(canvas)

    lines = lines_from(result)
    expect(lines.size).to eq(6)
    expect(lines[3]).to eq('255 0 0 0 0 0 0 0 0 0 0 0 0 0 0')
    expect(lines[4]).to eq('0 0 0 0 0 0 0 128 0 0 0 0 0 0 0')
    expect(lines[5]).to eq('0 0 0 0 0 0 0 0 0 0 0 0 0 0 255')
  end

  it 'splits lines longer than 70 characters' do
    color = a_color(1.0, 0.8, 0.6)
    canvas = a_canvas_full_of(10, 2, color)

    result = a_generator.generate(canvas)

    lines = lines_from(result)
    expect(lines.size).to eq(7)
    expect(lines[3]).to eq('255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204')
    expect(lines[4]).to eq('153 255 204 153 255 204 153 255 204 153 255 204 153')
    expect(lines[5]).to eq('255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204')
    expect(lines[6]).to eq('153 255 204 153 255 204 153 255 204 153 255 204 153')
  end

  it 'terminates by a newline' do
    canvas = a_canvas

    result = a_generator.generate(canvas)

    last_character = result[-1]
    expect(last_character).to eq("\n")
  end

  def a_generator
    Raytracer::PpmGenerator.new
  end

  def a_canvas_full_of(width, height, color)
    canvas = a_canvas(width, height)
    (0..height-1).each do |y|
      (0..width-1).each do |x|
        canvas.write(x, y, color)
      end
    end
    canvas
  end

  def any_width
    4
  end

  def any_height
    2
  end

  def lines_from(string)
    string.split("\n")
  end
end
