require 'spec_helper'

require_relative '../raytracer/sphere'
require_relative '../raytracer/ray'

describe 'A Sphere' do
  it 'is intersected by a ray at two points' do
    origin = a_point(0, 0, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(4.0)
    expect(intersections[1].position).to eq(6.0)
  end

  it 'is intersected by a ray at the tangent' do
    origin = a_point(0, 1, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(5.0)
    expect(intersections[1].position).to eq(5.0)
  end

  it 'is not intersected by a ray that misses it' do
    origin = a_point(0, 2, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections).to be_empty
  end

  it 'is intersected by a ray at its center' do
    origin = a_point(0, 0, 0)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(-1.0)
    expect(intersections[1].position).to eq(1.0)
  end

  it 'is intersected by a ray it is behind of' do
    origin = a_point(0, 0, 5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(-6.0)
    expect(intersections[1].position).to eq(-4.0)
  end

  it 'sets the object on intersections' do
    origin = a_point(0, 0, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere()

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].object).to eq(sphere)
    expect(intersections[1].object).to eq(sphere)
  end

  it 'does not transform by default' do
    sphere = a_sphere

    expect(sphere.transformation).to eq(identity_matrix)
  end

  it 'can be transformed' do
    sphere = a_sphere
    transformation = Raytracer::Transformations.translation(2, 3, 4)

    sphere.transform(transformation)

    expect(sphere.transformation).to eq(transformation)
  end

  it 'is intersected by a ray when scaled' do
    origin = a_point(0, 0, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere
    scaling = Raytracer::Transformations.scaling(2, 2, 2)
    sphere.transform(scaling)

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(2)
    expect(intersections[0].position).to eq(3)
    expect(intersections[1].position).to eq(7)
  end

  it 'is intersected by a ray when translated' do
    origin = a_point(0, 0, -5)
    direction = a_vector(0, 0, 1)
    ray = Raytracer::Ray.new(origin, direction)
    sphere = a_sphere
    translation = Raytracer::Transformations.translation(5, 0, 0)
    sphere.transform(translation)

    intersections = sphere.intersect(ray)

    expect(intersections.count).to eq(0)
  end

  def identity_matrix
    Raytracer::Matrix.from_array([
      [1, 0, 0, 0],
      [0, 1, 0, 0],
      [0, 0, 1, 0],
      [0, 0, 0, 1]
    ])
  end
end
